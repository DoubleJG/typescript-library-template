# [3.0.0](https://gitlab.com/DoubleJ-G/typescript-library-template/compare/v2.0.0...v3.0.0) (2022-06-18)


### Features

* new small feature ([22963e7](https://gitlab.com/DoubleJ-G/typescript-library-template/commit/22963e7ff9008f2ac18fbf3946d296fb820d2b55))
* removing file ([42495cb](https://gitlab.com/DoubleJ-G/typescript-library-template/commit/42495cb4055720ebe094273b972f5fc4ed5740b9))


### BREAKING CHANGES

* This removes file which may be required by some people

# [3.0.0-development.2](https://gitlab.com/DoubleJ-G/typescript-library-template/compare/v3.0.0-development.1...v3.0.0-development.2) (2022-06-18)


### Features

* new small feature ([22963e7](https://gitlab.com/DoubleJ-G/typescript-library-template/commit/22963e7ff9008f2ac18fbf3946d296fb820d2b55))

# [3.0.0-development.1](https://gitlab.com/DoubleJ-G/typescript-library-template/compare/v2.0.0...v3.0.0-development.1) (2022-06-18)


### Features

* removing file ([42495cb](https://gitlab.com/DoubleJ-G/typescript-library-template/commit/42495cb4055720ebe094273b972f5fc4ed5740b9))


### BREAKING CHANGES

* This removes file which may be required by some people

# [2.0.0](https://gitlab.com/DoubleJ-G/typescript-library-template/compare/v1.0.0...v2.0.0) (2022-06-18)


### Features

* new file ([9c7e0cc](https://gitlab.com/DoubleJ-G/typescript-library-template/commit/9c7e0cc71abaac4d387e527212581570a1c9d0d8))


### BREAKING CHANGES

* This new file is new

# 1.0.0 (2022-06-17)


### Bug Fixes

* **ci:** use correct node image ([bfe6061](https://gitlab.com/DoubleJ-G/typescript-library-template/commit/bfe606190190df3455454da0ac6a7fbec866be50))
* **deps:** install required dependencies ([36bddf0](https://gitlab.com/DoubleJ-G/typescript-library-template/commit/36bddf0bc7039f629cbae884e7fdb2e1a3aca7c4))


### Features

* **ci:** remove @semantic-release/npm ([d175243](https://gitlab.com/DoubleJ-G/typescript-library-template/commit/d1752430b1c49166e36b94f0a0af969484becc69))
* install sementaic-release ([9ed2c97](https://gitlab.com/DoubleJ-G/typescript-library-template/commit/9ed2c97c91fc9b706956dce45ad562df573f3e88))

# [1.0.0-development.2](https://gitlab.com/DoubleJ-G/typescript-library-template/compare/v1.0.0-development.1...v1.0.0-development.2) (2022-06-17)


### Features

* **ci:** remove @semantic-release/npm ([d175243](https://gitlab.com/DoubleJ-G/typescript-library-template/commit/d1752430b1c49166e36b94f0a0af969484becc69))
