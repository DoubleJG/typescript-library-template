module.exports = {
    "branches": [
        { "name": "master" },
        { "name": "development", "prerelease": true }
    ],
    "plugins": [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/changelog",
        "@semantic-release/git",
        "@semantic-release/gitlab"
    ]
}